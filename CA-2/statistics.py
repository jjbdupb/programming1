'''
Main Python file for the statistics functions for CA-2. contains one
function that will calculate the turn statistics over a n games of
beggar your neighbour for x number of players
'''

# imports
from beggar import beggar
import numpy


def statistics(Nplayers, Games):
    '''
    takes x number of players, and n number of games
    returns the turn statistcisc over n games of beggar your neighbour
    '''
    trial = 1
    stats_info = []
    # for n games, generate a list of turns for each game
    while trial < Games:
        game = beggar()
        turns = game.main(Nplayers, False)
        stats_info.append(turns)
        trial += 1

    # fetch the shortest, longest and calculate the average number of turns
    shortest = min(stats_info)
    longest = max(stats_info)
    average = numpy.mean(stats_info)

    return shortest, average, longest


if __name__ == '__main__':
    for i in range(2, 11):
        shortest, average, longest = statistics(i, 10000)
        print("for", i, "players")
        print("Shortest Game:", shortest)
        print("Longest Game:", longest)
        print("Average Game:", average)
        print("----------------------")
