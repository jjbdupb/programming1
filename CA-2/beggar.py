# imports
import random


class beggar():
    '''
    Single class file for runing a game of beggar your neigbour
    contains 4 functions

    entrypoints of beggar() and main()
    '''

    def __init__(self):
        '''
        initialise a list for a shuffled deck and the penalty cards
        '''
        self.standard_deck = [2, 2, 2, 2, 3, 3, 3, 3, 4, 4, 4, 4, 5, 5, 5, 5,
                              6, 6, 6, 6, 7, 7, 7, 7, 8, 8, 8, 8, 9, 9, 9, 9,
                              10, 10, 10, 10, 11, 11, 11, 11, 12, 12, 12, 12,
                              13, 13, 13, 13, 14, 14, 14, 14]
        self.penalty_card = [11, 12, 13, 14]
        random.shuffle(self.standard_deck)

    def beggar(self, Nplayers, deck, talkative):
        '''
        Takes a shuffled deck and the number of players and compleetes one game
        returns the number of turns taken
        '''
        players = []
        # create a list of the needed players
        while len(players) < Nplayers:
            players.append([])
        active_player = 0
        # deal the deck between the number of players
        for card in deck:
            players[active_player].append(card)
            active_player = (active_player + 1) % Nplayers
        turn = 0
        active_player = 0
        pile = []
        # while the game is not finished continue taking turns
        while self.finished(players) is False:
            if talkative is True:
                print("Turn:", turn)
                print("Pile:", pile)
                print("Current Player is:", active_player)
                print("Players:")
                for player in players:
                    print(*player)
                print("-------------------------")
            # take the turn for the current player
            players[active_player], pile, reward = self.take_turn(
                players[active_player], pile)
            # if there is a reward then give this reward to the previous player
            if len(reward) > 0:
                last_player = (turn - 1) % Nplayers
                players[last_player] = players[last_player] + reward
            # if the current player has no cards left
            # remove that player from the game, and update Nplayers
            if len(players[active_player]) == 0:
                del players[active_player]
                Nplayers -= 1
            # increment the turn and the current active player
            turn += 1
            active_player = turn % Nplayers
        if talkative is True:
            print("Game Finished")
            print(turn, "Turns were Taken")
        return turn

    def finished(self, players):
        '''
        Check if the game is finished by checking if one player has 52 cards
        if this is true then only one player remains
        '''
        for player in players:
            if len(player) == 52:
                return True
        return False

    def take_turn(self, player, pile):
        '''
        takes one turn for the given player, and current pile of cards
        returns the new state of the player, and pile
        '''
        reward = []
        # if there are allready cards in the pile, check if the last card
        # was a penalty card, if so, add the needed cards to the reward pile
        # for the previous player
        if len(pile) > 0:
            if pile[-1] in self.penalty_card:
                cards_to_pay = pile[-1] % 10
                while cards_to_pay > 0 and len(player) > 0:
                    next_card = player[0]
                    pile.append(next_card)
                    del player[0]
                    cards_to_pay -= 1
                    if pile[-1] in self.penalty_card:
                        return player, pile, reward
                reward = pile
                pile = []
                return player, pile, reward
            # the last card was not a penalty card
            # the player just adds their top card to the pile
            else:
                pile.append(player[0])
                del player[0]
                return player, pile, reward
        # Since the pile is empty, the active player
        # adds their current top card to the pile
        else:
            pile.append(player[0])
            del(player[0])
            return player, pile, reward

    def main(self, Nplayers, talkative):
        '''
        Returns the number of turns taken, for a game of Nplayers
        '''
        turn = self.beggar(Nplayers, self.standard_deck, talkative)
        return turn


if __name__ == '__main__':
    # get the number of players to play the game with from the command line
    # runs a game with that number of players
    print("How many players should the game have??")
    Nplayers = int(input("--> "))
    game = beggar()
    game.main(Nplayers, True)
