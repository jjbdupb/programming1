# imports
import random
import math


class riffle():
    '''
    single class file to simulate a riffle shuffle for CA-2

    Contains 6 functions, main entrypoints of riffle_once(), check_quality()
    riffle(), average_quality()
    '''

    def __init__(self):
        '''
        initalise test lists for function
        '''
        self.list_20 = list(range(20))
        self.list_greek = ['alpha', 'beta', 'gamma', 'delta', 'epsilon',
                           'zeta', 'eta', 'theta', 'iota', 'kappa',
                           'lambda', 'mu']
        self.list_50 = list(range(50))

    def riffle_once(self, lst):
        '''
        takes one list lst, and returns a list that has been shuffled once

        Splits the list into two equal lists to simulate a real riffle shuffle
        '''

        # split the list into two eqal lists
        lst_length = len(lst)
        mid_point = math.floor(lst_length / 2)
        lst_A = lst[:mid_point]
        lst_B = lst[mid_point:]
        new_lst = []

        # create shuffled list by picking with 50/50 chance
        # the next element from lists A and B
        while len(new_lst) < lst_length:
            rand = random.random()
            # check if there are elements in both A and B
            if len(lst_A) > 0 and len(lst_B) > 0:
                # randonly pick next element from one list, and add to new list
                # lists are waighted at 50/50
                if rand >= 0.5:
                    new_lst.append(lst_A[0])
                    del lst_A[0]
                if rand < 0.5:
                    new_lst.append(lst_B[0])
                    del lst_B[0]
            else:
                # if one list is empty
                # add rest of the remaining list to shuffled list
                if len(lst_A) > 0:
                    new_lst = new_lst + lst_A
                    break
                else:
                    new_lst = new_lst + lst_B
                    break
        return new_lst

    def riffle(self, lst, n):
        '''
        takes a list lst, and the number of time to riffle shuffle item
        returns list after n riffle shuffles
        '''
        new_lst = lst
        i = 0
        while i < n:
            new_lst = self.riffle_once(new_lst)
            i += 1
        return new_lst

    def check_shuffle(self, lst, new_lst):
        '''
        Takes origonal list lst, and shuffled list new_lst
        checks basic needs for a true shuffle,
        mainly that each element in lst, is in new_lst
        '''
        assert len(new_lst) == len(lst), "Sorted List is Shorter"
        for item in lst:
            if item not in new_lst:
                raise Exception("item lost while shuffling list")

    def quality(self, lst):
        '''
        checks how shuffled the list lst is on a scale from 0-1
        perfect shuffle is 0.5

        does this by checking the list for the number of pairs that are
        in accending order out of the total number of pairs.
        1 = pure acending list
        0 = pure decending list
        0.5 = random order
        '''
        pair_order = 0
        # check if pair is in accending order
        for i in range(len(lst) - 1):
            if lst[i] < lst[i + 1]:
                pair_order += 1
        quality = pair_order / len(lst)
        return quality

    def average_quality(self, n, trials):
        '''
        returns the average quality of shuffling a list of 50 elements n times
        over x trials
        '''
        i = 0
        quality = 0
        quality_tot = 0
        # over i number of trials, shuffle the list, n times
        # determine the quality, and calculate the mean quality
        while i < trials:
            shuffled_list = self.riffle(self.list_50, n)
            quality = self.quality(shuffled_list)
            quality_tot = quality_tot + quality
            i += 1
            mean_quality = quality_tot / i
        return mean_quality

    def main(self, trials):
        '''
        main function for running riffle class class for CA-2 problems
        '''
        # shuffle a list of 20 elemnts, and check the shuffle
        shuffled_list_20 = self.riffle_once(self.list_20)
        self.check_shuffle(self.list_20, shuffled_list_20)
        print("Using one Riffle", self.list_20, "-->", shuffled_list_20)
        print("quality of this shuffle is", self.quality(shuffled_list_20))
        # shuffle a list of the greek letters, and check the shuffle
        shuffled_list_greek = self.riffle_once(self.list_greek)
        self.check_shuffle(self.list_greek, shuffled_list_greek)
        print("using one Riffle", self.list_greek, "-->", shuffled_list_greek)
        # for n 1-15, check the quality of the shuffle of a list of 50 elements
        # for n number of riffles over x number of trials
        for i in range(1, 16):
            print("checking Qaulity of shuffle for {} times riffled, over {} trials".format(
                i, trials))
            print("Qaulity is", self.average_quality(
                i, trials), "after {} trials".format(trials))


if __name__ == '__main__':
    shuffling = riffle()
    shuffling.main(30)
