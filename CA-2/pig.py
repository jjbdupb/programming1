class piglatin():
    '''
    Main class for piglattin functions for CA-2

    contains four functions, main entry points are pig() and pigify()
    '''

    def __init__(self):
        '''
        initalise the list of vowles to check agaist
        and a dict of test words and answers to test functionality
        '''
        self.vowel_list = ['a', 'e', 'i', 'o', 'u']
        self.test_list = {'happy': 'appyhay', 'duck': 'uckday',
                          'glove': 'oveglay', 'evil': 'evilway',
                          'eight': 'eightway', 'yowler': 'owleryay',
                          'crystal': 'ystalcray'}

    # take a string and the location of vowel, returns a string
    # of the pigified word
    def pig_word_vowel(self, string, location):
        '''
        take a string and the location of vowel, returns a string
        of the pigified word
        '''
        string = string[location:] + string[:location] + 'ay'
        return string

    def pig(self, word):
        '''
        takes one word, and returns the pigified word
        '''
        vowel_location = None
        y_location = None
        # first find the location of any vowel, and the location of a y
        for i, char in enumerate(word):
            if char in self.vowel_list:
                vowel_location = i
                break
            if char == 'y':
                y_location = i
        # Check if y is in the word. If y is not in the word,
        # then only the location of vowels matters
        # if y is in the word, then logic is needed to determin if
        # y is acting as a vowel or a constanent
        if y_location is not None:
            if vowel_location is not None:
                if y_location < vowel_location and y_location > 0:
                    # since y is acting as a vowel, use y_location
                    # as vowel location in the pigify funtion
                    word = self.pig_word_vowel(word, y_location)
                else:
                    word = self.pig_word_vowel(word, vowel_location)
            else:
                # if no vowel is found, then y must act as a vowel
                word = self.pig_word_vowel(word, y_location)
        elif vowel_location is not None:
            if vowel_location > 0:
                # vowel not at begining of the word, so use pigify funciton
                # with vowel_location
                word = self.pig_word_vowel(word, vowel_location)
            else:
                # vowel at start of the word, thus add 'way' to the end
                word = word + 'way'
        else:
            # if word has no vowel, or y char,
            # return error state
            return -1
        return word

    def test_pig(self):
        '''
        fucntion to test pig(), uses a dict of known words and pig versions
        '''
        # itterate through the dict, and pigify the known word
        for item in self.test_list.keys():
            pig_item = self.pig(item)
            print(pig_item, item)
            # check piged word agaist the referace from the dict
            # raise error if not the case
            assert (pig_item == self.test_list[item]), "inbuilt tests failed"

    def pigify(self, string):
        '''
        Takes a string, and splits it into a list of words,
        pig's each word then re assembles the string
        '''
        pig_words = []
        # split string into list of words
        word_list = string.split()
        # for each word in list, run word through pig funciton
        # add pigged word into new list
        for i in word_list:
            pig_word = self.pig(i)
            if pig_word == -1:
                print("Function returned -1, word with no vowel found" +
                      " ... ignoring word")
                pig_word = i
            pig_words.append(pig_word)
        # take list of piged words, and reassemble into one string
        pig_string = str.join(' ', pig_words)
        return pig_string


if __name__ == '__main__':
    print('Enter a string to pigify..')
    pltn = piglatin()
    # get input strings from the commandline
    # two special strings
    # empty string exists the program
    # string 'test_pig' runs the inbuilt tests
    while True:
        inpt = input('--> ')
        if inpt is '':
            break
        if inpt == 'test_pig':
            pltn.test_pig()
            continue
        print(pltn.pigify(inpt))
