##############################################################################
#  FILE
#      $HeadURL: https://gitlab.com/jjbdupb/programming1/blob/master/goldenratio.py
#      $Author: jdpb
#
#  ORIGINAL AUTHOR
#      Joshua du Parc Braham
#
#  DESCRIPTION
#      Main Python file for the fibbonacci functions of CA - 1
#      for module ECM 1400
#
##############################################################################


# Takes one arg n: number to calculate ratio up to
# prints the ratio between two fibonacci numbers up to n
def goldenratio_sequence(n):
    fibonacci_numbers = [0, 1, 1]
    i = 1
    while i < n:
        next_fib = fibonacci_numbers[-1] + fibonacci_numbers[-2]
        ratio = fibonacci_numbers[-1] / fibonacci_numbers[-2]
        fibonacci_numbers.append(next_fib)
        i += 1
        print('Ratio', i, 'Is', ratio)


# Takes two args, tolerance: number to approximate φ to,
# maxnum, maximum number of fibonachi numbers that will be generated,
# prints the number of fibonachi numbers needed to
# approximate φ to within tollerance
# Also prints the approximated φ
def find_minimum_fibonacci_numbers(tolerance, maxnum):
    # initalise start point, referance goldenratio,
    # first fibonacci numbers, and the relevent
    # infomation with that start point
    goldenratio = 1.6180339887498948
    fibonacci_numbers = [0, 1, 1]
    fib_penult = 1
    ratio = 1
    n = 3

    # Check the ratio between the last two fibonachi numbers
    # and if needed generate the next one
    while abs(ratio - goldenratio) > tolerance:
        # exit out of function if maxnum is reached
        if n > maxnum:
            print('Maximum fibbonacci number generation reached...')
            print('Either reduce tolerance or raise maxnum ...')
            return
        # get the last and 2nd last fibonachi numbers without indexing lists
        # find the ratio between the two,
        # generate the next fibonachi number and set the 2nd last number
        # to the largest of the current loop.
        fib_last = max(fibonacci_numbers)
        ratio = fib_last / fib_penult
        fib_next = fib_last + fib_penult
        fib_penult = fib_last
        fibonacci_numbers.append(fib_next)
        n += 1
    # uses n -1 to fix off by one error in
    # getting the number of fibonachi numbers needed
    print(n - 1,
          'Fibonachi numbers were needed to approximate φ to within',
          tolerance, 'φ approx =', ratio)


if __name__ == '__main__':
    goldenratio_sequence(20)
    tolerance_list = [0.1, 10**-10, 10**-14, 10**-18]
    for tolerance in tolerance_list:
        find_minimum_fibonacci_numbers(tolerance=tolerance, maxnum=100)
