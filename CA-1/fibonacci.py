##############################################################################
#  FILE
#      $HeadURL: https://gitlab.com/jjbdupb/programming1/blob/master/fibonacci.py
#      $Author: jdpb
#
#  ORIGINAL AUTHOR
#      Joshua du Parc Braham
#
#  DESCRIPTION
#      Main Python file for the fibbonacci functions of CA - 1
#      for module ECM 1400
#
##############################################################################


# takes one arg n: the fibonachi numbers to find
# prints all fibonachi numbers up to n
def fibonacci(n):
    if n <= 2:
        print('The First three fibbonacci numbers are 0, 1, 1 by definition')
        return
    fibonacci_numbers = [0, 1, 1]
    i = 2
    while i < n:
        next_fib = fibonacci_numbers[-1] + fibonacci_numbers[-2]
        fibonacci_numbers.append(next_fib)
        i += 1
    print(fibonacci_numbers)


if __name__ == '__main__':
    fibonacci(20)
