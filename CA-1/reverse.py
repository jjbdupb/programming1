##############################################################################
#  FILE
#      $HeadURL: https://gitlab.com/jjbdupb/programming1/blob/master/reverse.py
#      $Author: jdpb
#
#  ORIGINAL AUTHOR
#      Joshua du Parc Braham
#
#  DESCRIPTION
#      Main Python file for the reverse string function of CA - 1
#      for module ECM 1400
#
##############################################################################


# Takes two args, String: the string to reverse,
# print_spaces: Bool to decide weather to
# print the reversed string with spaces
def print_reverse(String, print_spaces):

    reversed_string = ''
    # get String length do properly index for all lengths of string
    str_len = len(String)

    # itterate through string backwards, adding each letter to new string
    for i in range(1, str_len + 1):
        reversed_string = reversed_string + String[-i]

    if print_spaces is True:
        # reduce string length by 1
        # to fix off by one error in number of spaces
        str_len -= 1
        print('Reversed String with Spaces:\n')
        # itterate through letters in reversed string,
        # printing the letter and correct number of spaces
        for c in reversed_string:
            print(str_len * ' ' + c)
            str_len -= 1
    if print_spaces is False:
        print(' Reversed String Without Spaces:\n')
        for c in reversed_string:
            print(c)


if __name__ == '__main__':
    print_reverse("reversestring", print_spaces=False)
    print_reverse("reversestring", print_spaces=True)
