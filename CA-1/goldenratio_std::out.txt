josh@josh-XPS-15-9570 /opt/projects/programming1 (master) $ python3 goldenratio.py
Ratio 2 Is 1.0
Ratio 3 Is 2.0
Ratio 4 Is 1.5
Ratio 5 Is 1.6666666666666667
Ratio 6 Is 1.6
Ratio 7 Is 1.625
Ratio 8 Is 1.6153846153846154
Ratio 9 Is 1.619047619047619
Ratio 10 Is 1.6176470588235294
Ratio 11 Is 1.6181818181818182
Ratio 12 Is 1.6179775280898876
Ratio 13 Is 1.6180555555555556
Ratio 14 Is 1.6180257510729614
Ratio 15 Is 1.6180371352785146
Ratio 16 Is 1.618032786885246
Ratio 17 Is 1.618034447821682
Ratio 18 Is 1.6180338134001253
Ratio 19 Is 1.618034055727554
Ratio 20 Is 1.6180339631667064
6 Fibonachi numbers were needed to approximate φ to within 0.1 φ approx = 1.6666666666666667
27 Fibonachi numbers were needed to approximate φ to within 1e-10 φ approx = 1.6180339886704431
37 Fibonachi numbers were needed to approximate φ to within 1e-14 φ approx = 1.6180339887498896
42 Fibonachi numbers were needed to approximate φ to within 1e-18 φ approx = 1.618033988749895