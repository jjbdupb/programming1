##############################################################################
#  FILE
#      $HeadURL: https://gitlab.com/jjbdupb/programming1/blob/master/longest.py
#      $Author: jdpb
#
#  ORIGINAL AUTHOR
#      Joshua du Parc Braham
#
#  DESCRIPTION
#      Main Python file for the longest word function of CA - 1
#      for module ECM 1400
#
##############################################################################


# Takes one arg, Filepath: path to the file read.
# Returns a list of all the words in given file
def read_words(FilePath):
    wordlist = []
    with open(FilePath, 'r') as file:
        wordlist = file.read().split()
        file.close()
    return wordlist


# Takes one arg, words: list of strings
# prints the first longest string in given list
def longest(words):
    print('First Longest word in File:')
    print(max(words, key=len) + '\n')


# Takes one arg, words: list of strings
# prints all strings with longest length
def all_longest(words):
    print('All of the longest words in file:')
    longest_length = len(max(words, key=len))
    for word in words:
        if len(word) == longest_length:
            print(word)


if __name__ == '__main__':
    words = read_words('words.txt')
    longest(words)
    all_longest(words)
