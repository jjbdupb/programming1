'''
Wordchain file, contains one calss chain_finder() with 2 methods
main entrypoints of main() and longest_chain()
'''


class chain_finder():
    def __init__(self):
        self.filenames = ['animals.txt', 'lotr.txt']

    def longest_chain(self, chain, _list, longest=[]):
        '''
        for a given list of words _list and a current chain
        recursivly finds the longest chain of words in _list
        where the last letter of each word is the same as the first letter
        of the next word
        '''

        # filer _list so only words not in chain are in the possible vocab
        # prevents repeats of words
        _list = list(filter(lambda word: word not in chain, _list))
        # check throug all possible permutations of the next word
        # due to the recustion this will check all possible valid list of words
        for word in [word for word in _list if word[:1].lower() == chain[-1]]:
            result = self.longest_chain(word, _list)
            if len(result) > len(longest):
                longest = result
        return [chain] + longest

    def main(self):
        '''
        main function, for each file in filenames
        returns the longest valid chain of words from each file
        '''
        for name in self.filenames:
            _list = []
            _dict = {}
            with open(name, 'r') as file:
                _lines = file.readlines()
                for word in _lines:
                    _list.append(word.strip())
            for word in _list:
                result = self.longest_chain(word, _list)
                _dict[len(result)] = result
            key = max(_dict.keys())
            print(_dict[key])


if __name__ == '__main__':
    finder = chain_finder()
    finder.main()
