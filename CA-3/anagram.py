'''
Anagram file, contains one class, anagram(), with 4 methods
main entrypoints of make_anagram_dict() and main()
'''


class anagram():
    def __init__(self):
        with open("words.txt", 'r') as file:
            self.words_list = file.readlines()
            self.words_list = [x.strip() for x in self.words_list]
            file.close

    def make_anagram_dict(self, _list):
        '''
        Takes a lis of words _list, and returns a dict of
        anagrams in the form of 'char_list': [list of words]
        for the purpose of anagrams a
        capital letter is equal to lowercase letter
        '''
        ana_dict = {}
        for word in _list:
            char_list = []
            for char in word:
                char_list.append(char.lower())
            char_list = sorted(char_list)
            char_list = ''.join(char_list)
            if char_list in ana_dict:
                ana_dict[char_list].append(word)
            else:
                ana_dict[char_list] = [word]
        return ana_dict

    def longest(self, _dict):
        '''
        Takes anagram dict: _dict, and returns
        the longest words with atleast one anagram
        '''
        long_len = 0
        for key in _dict.keys():
            ana_length = len(_dict[key])
            if ana_length >= 2:
                word_length = len(key)
                if word_length > long_len:
                    long_len = word_length
                    longest = key
        return _dict[longest]

    def most_anagrams(self, _dict):
        '''
        takes anagram dict: _dict, and returns
        the longest list of anagrams
        '''
        longest = 0
        for key in _dict.keys():
            length = len(_dict[key])
            if length > longest:
                longest = length
                long_key = key
        return _dict[long_key]

    def main(self):
        _dict = self.make_anagram_dict(self.words_list)
        print(_dict)
        print(self.longest(_dict))
        print(self.most_anagrams(_dict))


if __name__ == '__main__':
    ana = anagram()
    ana.main()
