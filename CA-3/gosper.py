'''
Gosper file, contains one class gosper() with 3 methods
main entrypoints of Gosper_draw() and main()
'''

from follow import follow


class gosper():

    def gosper(self, S, n, i=0):
        '''
        recusive function that for a given string S and n iterations
        will return a string that the gosper rules
        have been applied to n times
        '''
        i += 1
        if i >= n:
            self.S = S
            return
        new_S = []
        for char in S:
            if char == 'E':
                new_S.append('ELFLLFRERREERFL')
            if char == 'F':
                new_S.append('RELFFLLFLERRERF')
            if char == 'L':
                new_S.append('L')
            if char == 'R':
                new_S .append('R')
        S = ''.join(new_S)
        self.gosper(S, n, i)

    def gosper_draw(self, S, n, step=10, angle=60):
        '''
        for a given starting string S and n iterations,
        draws a gosper curve of the resulting string
        '''
        self.gosper(S, n)
        S = self.S
        turtle = follow()
        turtle.follow(S, step, angle)

    def main(self):
        '''
        Main function for file, draws a gosper curve with start point 'E'
        and iteration length 5
        '''
        self.gosper_draw('E', 5)


if __name__ == '__main__':
    gos = gosper()
    gos.main()
