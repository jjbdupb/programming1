'''
Histagram file, contains 1 class with one method histogram()
'''


class histagram():
    def histogram(y, width):
        '''
        takes a list of values y, and a int width
        prints a histagram using values from y,
        with a max withds of width
        '''
        max_val = max(y)
        for i, val in enumerate(y):
            stars = round((val / max_val) * width)
            print(i, '*' * stars, val)


if __name__ == '__main__':
    vals = [12.5, 6.4, 10, 7.6, 8, 13]
    histagram.histogram(vals, width=30)
