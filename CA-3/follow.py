'''
Follow file, contains one class follow() with 2 methods
follow() and main(), used to test turtle libary
'''

from turtle import *


class follow():
    def __init__(self):
        color('blue', 'white')
        delay(0)

    def follow(self, S, step=10, angle=30):
        '''
        Draws a line from a String with the folliwng rules
        F, E move forward
        L Turn left
        R turn right
        '''
        begin_fill()
        for char in S:
            if char in ['F', 'E']:
                forward(step)
            if char is 'L':
                left(angle)
            if char is 'R':
                right(angle)
        end_fill()
        done()

    def main(self):
        '''
        main method, ueses a test string to test follow method
        '''
        string = "FLERFLERFLERFFERLL"
        self.follow(string)


if __name__ == '__main__':
    fol = follow()
    fol.main()
