'''
Anaquery file, contains one class anaqueary() with two methods
main entrypoints of queary() and main()
'''
from anagram import anagram


class anaqueary():
    def __init__(self):
        with open("words.txt", 'r') as file:
            '''
            generates an anagram dict using a list of words from words.txt
            '''
            self.words_list = file.readlines()
            self.words_list = [x.strip() for x in self.words_list]
            file.close
        anag = anagram()
        self.anagram_dict = anag.make_anagram_dict(self.words_list)

    def queary(self, word):
        '''
        takes single string word and returns a list of its anagrams
        if word has no anagrams returns None
        '''
        letter_list = ''.join(sorted(list(word)))
        if letter_list in self.anagram_dict:
            return self.anagram_dict[letter_list]
        else:
            return None

    def main(self):
        '''
        Main function takes input from comandline
        returns the anagrams of the input
        '''
        print("Type a word to find its anagrams")
        while True:
            word = input('--> ').lower()
            if word == '':
                break
            anagram_list = self.queary(word)
            if anagram_list is not None:
                print("Found the following anagrams")
                print(anagram_list)
            else:
                print("No anagrams found :(")


if __name__ == '__main__':
    anaq = anaqueary()
    anaq.main()
