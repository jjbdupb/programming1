'''
Anahist file, contains one class anahist() with 3 methods
main entrypoints of draw_hist() and main()
'''
import math
from histagram import histagram
from anagram import anagram


class anahist():
    def __init__(self):
        '''
        load list of words from words.txt
        '''
        with open("words.txt", 'r') as file:
            self.words_list = file.readlines()
            self.words_list = [x.strip() for x in self.words_list]
            file.close

    def draw_hist(self, val_data, width):
        '''
        Takes a list of val_Data and the width and
        uses histogram fuction to print a histogram with
        given data
        '''
        histagram.histogram(val_data, width)

    def get_hist_data(self):
        '''
        uses list of words from words.txt and makes a dict
        using method from anagram() uses this data to
        generate a list of value_Data for a histogram
        '''
        anag = anagram()
        val_data = []
        values = {}
        _dict = anag.make_anagram_dict(self.words_list)
        lengths = [len(anagrams) for anagrams in _dict.values()]

        for _len in lengths:
            if _len >= 2:
                if _len in values:
                    values[_len] += 1
                else:
                    values[_len] = 1
        values = dict(sorted(values.items()))
        print(values)
        for i in range(2, len(values.values()) + 2):
            val_data.append(math.log10(values[i]))
        return(val_data)

    def main(self):
        '''
        main funcion, generates values for a histogram
        and uses draw_hist() to print a histagram with this data
        '''
        vals = self.get_hist_data()
        self.draw_hist(vals, 40)


if __name__ == '__main__':
    hist = anahist()
    hist.main()
